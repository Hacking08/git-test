module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
      pkg: grunt.file.readJSON('package.json'),
      copy:{
        filesBootStrapCSS: {flatten: true, expand: true, cwd: 'node_modules/bootstrap/',src: ['**/*.css'], dest: 'www/assets/css', filter: 'isFile'},

        filesBootStrapJS: {flatten: true, expand: true, cwd: 'node_modules/bootstrap/',src: ['**/*.js'], dest: 'www/assets/js/', filter: 'isFile'},

        filesJqueryJS: {flatten: true, expand: true, cwd: 'node_modules/jquery',src: ['**/*.js'], dest: 'www/assets/js', filter: 'isFile'}
      },
      connect: {
        server: {
          options: {
            port: 8000,
            protocol:"http",
            hostname:"localhost",
            base: {
              path: '.',
              options: {
                index: "tpl/src/template/index.html"
              }},
            directory: null,
            open: true,
            keepalive: true
          }
        }
      }
    });
  
    

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-connect');
  
    // Default task(s).
    grunt.registerTask('default', ['copy']);
    
  
  };