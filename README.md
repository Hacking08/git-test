### >>Test Project<< ###

## Česky
Tenhle projekt umí kopírovat a připojit se na localhost. 

Přikaz **grunt** nebo **grunt copy** =>
# >Kopíruje všechny **css** a **js** z [node_modules/bootstrap] do [www/assets/].

Přikaz **grunt connect** =>
# >Připojí se na [localhost:8000] a otevře webovou stránku kde mužeme dělat změny.



## English
This project can copy and connect to localhost. 

Command **grunt** or **grunt copy** =>
# >Copy all **css** and **js** from [node_modules/bootstrap] to [www/assets/].

Command **grunt connect** =>
# >It connect to [localhost:8000] and opens the web page where we can make changes.
